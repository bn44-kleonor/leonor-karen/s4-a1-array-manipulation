// WD004-S3-A1
//Push as	a2-js-control-structure-1

/*================================================================================
MODULE: WD004-S3-A1  

Create a variable `day` and assign a day to it. Using switch-case:
If it's
Monday, output in the console "You need to wear Red",
Tuesday:  "You need to wear Orange",
Wednesday: "You need to wear Yellow",
Thursday: "You need to wear Green",
Friday: "You need to wear Blue",
Saturday: "You need to wear Indigo",
Sunday: "You need to wear Violet",
else "You should wear black".

Push as: a2-js-control-structures-1
==================================================================================*/



// let day = prompt("What day is today?")

// switch (day) {
// 	case 'Monday':
// 		alert("You need to wear Red");
// 		break;
// 	case 'Tuesday':
// 		alert("You need to wear Orange");
// 		break;
// 	case 'Wednesday':
// 		alert("You need to wear Yellow");
// 		break;
// 	case 'Thursday':
// 		alert("You need to wear Green");
// 		break;
// 	case 'Friday':
// 		alert("You need to wear Blue");
// 		break;
// 	case 'Saturday':
// 		alert("You need to wear Indigo");
// 		break;
// 	case 'Sunday':
// 		alert("You need to wear Violet");
// 		break;
// 	default:
// 		alert("You need to wear black");
// 		break;
// }



// streched goal (ignore case) e.g. monday, MONday, mONDay (etc)
// solution: make all the value of case be lower case. then, covert all the input of user to all lower case using "toLowerCase()"

// let day = prompt("What day is today?")

// switch (day.toLowerCase()) {
// 	case 'monday':
// 		alert("You need to wear Red");
// 		break;
// 	case 'tuesday':
// 		alert("You need to wear Orange");
// 		break;
// 	case 'wednesday':
// 		alert("You need to wear Yellow");
// 		break;
// 	case 'thursday':
// 		alert("You need to wear Green");
// 		break;
// 	case 'friday':
// 		alert("You need to wear Blue");
// 		break;
// 	case 'saturday':
// 		alert("You need to wear Indigo");
// 		break;
// 	case 'sunday':
// 		alert("You need to wear Violet");
// 		break;
// 	default:
// 		alert("You need to wear black");
// 		break;
// }




//-------------- ans Ms. Joyce July 12 2019

// Activity 1 ==================================================================================
//--------- normal activity 
// let day = prompt("What day is today?")

// switch (day) {
// 	case 'monday':
// 		alert(`It's ${day}; You need to wear Red`);
// 		break;
// 	case 'tuesday':
// 		alert(`It's ${day}; You need to wear Orange`);
// 		break;
// 	case 'wednesday':
// 		alert(`It's ${day}; You need to wear Yellow`);
// 		break;
// 	case 'thursday':
// 		alert(`It's ${day}; You need to wear Green`);
// 		break;
// 	case 'friday':
// 		alert(`It's ${day}; You need to wear Blue`);
// 		break;
// 	case 'saturday':
// 		alert(`It's ${day}; You need to wear Indigo`);
// 		break;
// 	case 'sunday':
// 		alert(`It's ${day}; You need to wear Violet`);
// 		break;
// 	default:
// 		alert(`It's ${day}; You need to wear black`);
// 		break;
// }


//--------- streched goal (ignore case) e.g. monday, MONday, mONDay (etc)


// day = prompt("What day is today?")

// switch (day.toLowerCase()) {			// use .toLowerCase()
// 	case 'monday':
// 		alert(`It's ${day}; You need to wear Red`);
// 		break;
// 	case 'tuesday':
// 		alert(`It's ${day}; You need to wear Orange`);
// 		break;
// 	case 'wednesday':
// 		alert(`It's ${day}; You need to wear Yellow`);
// 		break;
// 	case 'thursday':
// 		alert(`It's ${day}; You need to wear Green`);
// 		break;
// 	case 'friday':
// 		alert(`It's ${day}; You need to wear Blue`);
// 		break;
// 	case 'saturday':
// 		alert(`It's ${day}; You need to wear Indigo`);
// 		break;
// 	case 'sunday':
// 		alert(`It's ${day}; You need to wear Violet`);
// 		break;
// 	default:
// 		alert(`It's ${day}; You need to wear black`);
// 		break;
// }



// Activity 2 ==================================================================================

/*
MODULE: WD004-S3-A2
  1. Display in the console all numbers FROM -10 to 20
  2. Display all odd numbers BETWEEN 10 and 40
  3. Create a variable `evenSum` and assign it an initial value of 0.
      Create a variable `oddSum` and assign it an initial value of 0.
      Iterate through all integers from 0 to 50.
      Compute for the sum of all odd integers.
      Compute for the sum of all even integers.
      Display the sums.

      EXPECTED OUTPUT: 
      The sum of all even numbers from 0 to 50 is 650;
      The sum of all odd numbers from 0 to 50 is 625;
*/

  console.log("1. Display in the console all numbers FROM -10 to 20")
  let num = -10;
  while (num <= 20) {
    	console.log(num);
   		num++;
  }

  console.log("2. Display all odd numbers BETWEEN 10 and 40")
  num = 10;
  num++;
  while(num <= 40) {
  		console.log(num)
  		num += 2;
  }


console.log("3. evenSum and oddSum")
let evenSum = 0;
let oddSum = 0;

for(let i=0; i<=50; i++) {
  if (i%2 !== 0) {
    oddSum += i;
  } else {
    evenSum += i;
  }
}

console.log(`The sum of all even numbers from 0 to 50 is ${evenSum}`);
console.log(`The sum of all even numbers from 0 to 50 is ${oddSum}`);
