/*=================================================
MODULE: WD004-S4-ARRAY MANIPULATION

ARRAYS
- Stores multiple relative values in a single variable.
- values can have different data types
===================================================*/

// INITIALIZATION
let students = [];
let students2 = new Array(); //uncommon


// ARRAYS CAN HAVE DIFFERENT DATA TYPES
let arr = ["string", true, 1, null, undefined];
let movies = [
	"Godfather",
	"Mission Impossible",
	"Avengers",
	"Venom",
	"The Nun"
		];


// >>> On your own, initialize a products array with at least 7 items.
let products = [
	"junk food",
	"tea",
	12345,
	true,
	NaN
		];


// DATA TYPE
console.log("Data Type of Array");
console.log(typeof movies);


// ARRAY HAS .length PROPERTY
console.log(".length property of array");
console.log(movies.length);


// >>> On your own, display the number of items of the products array that you made.
console.log(".length property of array products");
console.log(typeof products);
console.log(products.length);


// DISPLAY INDIVIDUAL ITEMS/VALUES VIA INDEX NUMBER
console.log("Index of array movies");
console.log(movies[0]);
console.log(movies[1]);
console.log(movies[2]);
console.log(movies[3]);
console.log(movies[4]);
console.log(movies[5]);		//undefined because array length 5 only (indices 0-4)


// >>> On your own, display the individual items inside your products array via their index number.
console.log("Index of array products");
console.log(products[0]);
console.log(products[1]);
console.log(products[2]);
console.log(products[3]);
console.log(products[4]);
console.log(products[5]);


// DISPLAY INIDIVIDUAL VALUES INSIDE AN ARRAY
console.log("Display values on array movies via .length");
console.log(movies[movies.length-1]);
console.log(movies[movies.length-2]);
console.log(movies[movies.length-3]);
console.log(movies[movies.length-4]);
console.log(movies[movies.length-5]);


// >>> On your own, display the individual values inside your products array using .length.
console.log("Display values on array products via .length");
console.log(products[products.length-1]);
console.log(products[products.length-2]);
console.log(products[products.length-3]);
console.log(products[products.length-4]);
console.log(products[products.length-5]);


// CHANGE A VALUE
console.log("Change value in an array");
movies[3] = "Toy Story 3";
console.log(movies);


// >>> On your own, change the value of at least three items inside your products array.
console.log("Change values of 3 items in products array");
products[2] = 5;
products[3] = null;
products[4] = "sample product";
console.log(products);


/*================================================
ARRAY METHODS

Note: Provide own definitions after teacher's demonstration.

1. .indexOf - returns the index of the value that you passed.
2. .push - add the new value at the end of the array.
3. .pop  - remove the last value of the array but returns (displayed in console) the removed value.
4. .unshift - insert value on the beginning of array (index 0).
5. .shift - remove the first value of the array (index 0) but returns (displayed in console) the removed value.
6. splice - removes or replaces an item in the array
7. slice - returns values from the array as indicated by the start index and then no. of items beginning from the mentioned index. (arguments: start, no. of items)
8. sort - sort items in array alphabetically
9. reverse - reverse the order of items in an array
10. concat - combines/joins the values of arrays

===================================================*/

console.log("ARRAY METHODS")

// >>> On your own, locate the index of at least 3 items in your array; locate the index of an item that doesn't exist.
console.log("INDEXOF")
let colors = [
		"red",
		"orange",
		"yellow",
		"blue",
	]
console.log(colors);
console.log("indexOf items in colors array");
console.log(colors.indexOf("red"));
console.log(colors.indexOf("orange"));
console.log(colors.indexOf("yellow"));
console.log(colors.indexOf("blue"));
console.log(colors.indexOf("color not in array"));		//-1 if value in not found in array

console.log(products);
console.log("indexOf items in products array");
console.log(products.indexOf("junk food"));
console.log(products.indexOf("tea"));
console.log(products.indexOf(5));
console.log(products.indexOf(null));
console.log(products.indexOf("sample product"));


// >>> On your own, apply push, pop, unshift and shift methods to your products array.
// PUSH - add the new value at the end of the array.
console.log("PUSH");
console.log(colors.push("green"));
console.log(colors);

//POP - remove the last value of the array but returns (you'll only see displayed in console) the removed value.
console.log("POP");
console.log(colors.pop());
console.log(colors);

// UNSHIFT -  insert value on the beginning of array (index 0).
console.log("UNSHIFT");
console.log(colors.unshift("indian red"));
console.log(colors);

// SHIFT - remove the first value of the array (index 0) but returns (displayed in console) the removed value.
console.log("SHIFT");
console.log(colors.shift());
console.log(colors);

//--- products array
// PUSH - add the new value at the end of the array.
console.log("PUSH products array");
console.log(products.push("paper bag"));
console.log(colors);

//POP - remove the last value of the array but returns (you'll only see displayed in console) the removed value.
console.log("POP products array");
console.log(products.pop());
console.log(products);

// UNSHIFT -  insert value on the beginning of array (index 0).
console.log("UNSHIFT products array");
console.log(products.unshift("drinks"));
console.log(products);

// SHIFT - remove the first value of the array (index 0) but returns (displayed in console) the removed value.
console.log("SHIFT products array");
console.log(products.shift());
console.log(products);


// >>> On your own, apply splice and slice methods to your products array.

// SPLICE - removes or replaces an item in the array
// console.log("SPLICE");
// console.log(colors);
// colors.splice(colors.indexOf("orange"), 1, "yello orange"); 	//2-3 arguments: (index no.) , (no. of items), (the replacements)
// console.log(colors);

//or to be more readable
console.log("SPLICE");
console.log(colors);
let orangeIndex = colors.indexOf("orange");
colors.splice(orangeIndex, 1, "yello orange"); 	//2-3 arguments: (index no.) , (no. of items), (the replacements)
console.log(colors);


// SLICE - returns values from the array as indicated by the start index and then no. of items beginning from the mentioned index. (arguments: start, no. of items)
console.log("SLICE");
console.log(colors);
let x = colors.slice(0,2);	// arguments: start, no. of items
console.log(x);
console.log(colors);


console.log("SLICE of products array");
console.log(products);
x = products.slice(0,2);	// arguments: start, no. of items
console.log(x);
console.log(products);


// >>> On your own, apply sort and reverse methods in your array.
console.log("SORT");
console.log(colors.sort());

console.log("REVERSE");
let nums = [1,2,4,8,3];
console.log(nums.reverse());

console.log("SORT AND REVERSE");
console.log(nums.sort().reverse());		//see syntax
console.log(colors);
console.log(nums);



//PRODUCTS array
console.log("SORT");
console.log(products.sort());

console.log("REVERSE");
console.log(products.reverse());




// >>> On your own, initialize a new array with at least 3 items and then concatinate it with your products array.

console.log("CONCAT");
console.log(colors.concat(nums));
console.log(colors);
console.log(nums);

console.log("CONCAT arrays products and colors");
console.log(colors.concat(products));
console.log(colors);
console.log(products);


/*================================================

Can we programatically display all items in an array?
through ARRAY ITERATION
1. FOR LOOP
2. forEach
3. Map




===================================================*/

// ____FOR LOOP____
console.log("FOR LOOP to display value of array");
for(let m=0; m < movies.length; m++) {
	console.log(movies[m]);
}


// ___FOREACH___
console.log("FOREACH to display value of array");

// old syntax
// movies.forEach(function(movie) {				// has anonymous function - won't be reused else where (will not be called else where)
// 	console.log(movie);
// })

//new syntax (shorter)
movies.forEach(movie => console.log(movie));		// function term was removed because it has no name because it won't be used else where


// ___MAP___
console.log("MAP");
movies.map(movie=> console.log(movie));

console.log(nums)
nums.map(num => nums === 0 ? 
		console.log(`${num} is even`) : console.log(`${num} is odd`)
);






// >>> On your own, apply above methods to display the contents of your products array.

