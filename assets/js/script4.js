/* ========================================================
MODULE: WD004-S4-ARRAY MANIPULATION (ACTIVITY)

1. Initialize a "tasks" array with the strings: eat,sleep, & code stored in it.
2. Declare a variable "count".
3. Initialize a "displayTasks" function that CONTAINS the ff functionalities:
  a) it should iterate all the elements of the tasks array
  b) it should output the following:
      Task 1: eat
      Task 2: sleep
      Task 3: code

4. Initialize an "addTask" function that accepts "newTask" as its parameter and CONTAINS 
   the ff functionalites:
  a)  it should add the value of newTask at the end of the array
  b)  it should output the strings that you will add in step 5 
      so that your console will now display the following:
      Task 1: eat
      Task 2: sleep
      Task 3: code
      You added: go home
      Task 1: eat
      Task 2: sleep
      Task 3: code
      Task 4: go home
      You added: enjoy the weekend
  c)  it should then call back the function that DISPLAYS the updated list of tasks. 
      Yes, you can call a different function WITHIN a function.

5. pass the following as arguments to addTask function:
  - go home
  - enjoy the weekend

6. Initialize a "deleteTask" function that CONTAINS the ff functionalities:
  a) it should output in the console the string you removed (i.e., the task you removed) 
     so that your console will now display the following:
    Task 1: eat
    Task 2: sleep
    Task 3: code
    You added: go home
    Task 1: eat
    Task 2: sleep
    Task 3: code
    Task 4: go home
    You added: enjoy the weekend
    Task 1: eat
    Task 2: sleep
    Task 3: code
    Task 4: go home
    Task 5: enjoy the weekend
    You removed: enjoy the weekend
    Task 1: eat
    Task 2: sleep
    Task 3: code
    Task 4: go home
  b) it should delete the the LAST TASK in the "tasks" array
  c) it should then call back a function that DISPLAYS the updates list of tasks
  
SAVE AS: s4-a1-array-manipulation

STRECH GOAL:
deleteTask function should delete the item that you wish to delete. 
Check your notes. We covered this! :-)

===========================================================*/

//1. Initialize a "tasks" array with the strings: eat,sleep, & code stored in it.
let tasks = ["eat",
      "sleep",
      "code"
      ];


//2. Declare a variable "count".
let count;


/*3. Initialize a "displayTasks" function that CONTAINS the ff functionalities:
  a) it should iterate all the elements of the tasks array
  b) it should output the following:
      Task 1: eat
      Task 2: sleep
      Task 3: code*/

function displayTasks() {
  for(count=0; count < tasks.length; count++) {
    console.log("Task " + `${count+1}` + ": " + tasks[count]);
  }
}

displayTasks();



/*4. Initialize an "addTask" function that accepts "newTask" as its parameter and CONTAINS the ff functionalites:
  a)  it should add the value of newTask at the end of the array
  b)  it should output the strings that you will add in step 5 
      so that your console will now display the following:
      Task 1: eat
      Task 2: sleep
      Task 3: code
      You added: go home
      Task 1: eat
      Task 2: sleep
      Task 3: code
      Task 4: go home
      You added: enjoy the weekend
  c)  it should then call back the function that DISPLAYS the updated list of tasks. 
      Yes, you can call a different function WITHIN a function.*/

let newTask;
function addTask(newTask) {
    console.log("You added: " + newTask)
    tasks.push(newTask);
    displayTasks();
    
}


/*5. pass the following as arguments to addTask function:
  - go home
  - enjoy the weekend */
addTask("go Home");
addTask("enjoy the weekend");


/*6. Initialize a "deleteTask" function that CONTAINS the ff functionalities:
  a) it should output in the console the string you removed (i.e., the task you removed) 
  b) it should delete the the LAST TASK in the "tasks" array
  c) it should then call back a function that DISPLAYS the updates list of tasks*/

// function deleteTask() {
//     console.log("You removed: " + tasks.pop());
//     displayTasks();
// }

// deleteTask();



//strech goal: deleteTask function should delete the item that you wish to delete. 

let taskToDelete;
function deleteTask(taskToDelete) {
    //console.log("what's in taskToDelete = " + taskToDelete);
    let taskIndex = tasks.indexOf(taskToDelete);
    //console.log("what's in taskIndex = " + taskIndex);
    console.log("You removed: " + taskToDelete);
    tasks.splice(taskIndex ,1);
    displayTasks();
}

deleteTask("sleep");
